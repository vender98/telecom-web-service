package com.vender98.telecomWebService.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
public class GeneralTariffPlan {
    @Id
    private long id;
    private String name;
    private String price;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "generalTariffPlan", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Characteristic> characteristics;
    @OneToMany(mappedBy = "generalTariffPlan")
    private Set<CustomerTariffPlan> customerTariffPlans;

    private int megabytes;
    private int minutes;
    private int sms;

    public GeneralTariffPlan(long id, String name, String price,
                             Set<Characteristic> characteristics, int megabytes, int minutes,
                             int sms) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.characteristics = characteristics;
        this.megabytes = megabytes;
        this.minutes = minutes;
        this.sms = sms;
    }

    public GeneralTariffPlan() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Set<Characteristic> getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(Set<Characteristic> characteristics) {
        this.characteristics = characteristics;
    }

    public int getMegabytes() {
        return megabytes;
    }

    public void setMegabytes(int megabytes) {
        this.megabytes = megabytes;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getSms() {
        return sms;
    }

    public void setSms(int sms) {
        this.sms = sms;
    }
}
