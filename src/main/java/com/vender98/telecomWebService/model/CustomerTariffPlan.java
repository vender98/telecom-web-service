package com.vender98.telecomWebService.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@PrimaryKeyJoinColumn(name = "line_number")
public class CustomerTariffPlan {
    @Id
    private long lineNumber;
    private Timestamp connectionDate;
    private String name;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "customerTariffPlan", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Package> packages;
    @ManyToOne
    @JoinColumn(name = "generalTariffPlanId", referencedColumnName = "id")
    private GeneralTariffPlan generalTariffPlan;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Package> getPackages() {
        return packages;
    }

    public void setPackages(Set<Package> packages) {
        this.packages = packages;
    }

    public Timestamp getConnectionDate() {
        return connectionDate;
    }

    public void setConnectionDate(Timestamp connectionDate) {
        this.connectionDate = connectionDate;
    }

    public long getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(long lineNumber) {
        this.lineNumber = lineNumber;
    }

    public void setGeneralTariffPlan(GeneralTariffPlan generalTariffPlan) {
        this.generalTariffPlan = generalTariffPlan;
    }
}
