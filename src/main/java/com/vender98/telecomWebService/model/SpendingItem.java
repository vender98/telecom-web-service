package com.vender98.telecomWebService.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
public class SpendingItem {
    @Id
    private long id;
    private long itemTypeId;
    private long lineNumber;
    private String name;
    private int amount;
    private String details;
    private Timestamp date;
    private String description;

    public SpendingItem() {}

    public SpendingItem(String name, int amount, long itemTypeId) {
        this.name = name;
        this.amount = amount;
        this.itemTypeId = itemTypeId;
    }

    public SpendingItem(int amount, String details, Timestamp date, String description) {
        this.amount = amount;
        this.details = details;
        this.date = date;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(long lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(long itemTypeId) {
        this.itemTypeId = itemTypeId;
    }
}
