package com.vender98.telecomWebService.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Package {
    @Id
    private String type;
    private int totalCapacity;
    private int remainingCapacity;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "lineNumber")
    private CustomerTariffPlan customerTariffPlan;

    public Package(){}

    public Package(String type, int totalCapacity, int remainingCapacity, CustomerTariffPlan customerTariffPlan) {
        this.type = type;
        this.totalCapacity = totalCapacity;
        this.remainingCapacity = remainingCapacity;
        this.customerTariffPlan = customerTariffPlan;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTotalCapacity() {
        return totalCapacity;
    }

    public void setTotalCapacity(int totalCapacity) {
        this.totalCapacity = totalCapacity;
    }

    public int getRemainingCapacity() {
        return remainingCapacity;
    }

    public void setRemainingCapacity(int remainingCapacity) {
        this.remainingCapacity = remainingCapacity;
    }
}
