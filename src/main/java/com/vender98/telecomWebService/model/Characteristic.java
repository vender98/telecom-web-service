package com.vender98.telecomWebService.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Characteristic {
    @Id
    private long id;
    private String type;
    private String generalInfo;
    private String description;
    @ManyToOne(optional = false)
    @JoinColumn(name = "generalTariffPlanId", referencedColumnName = "id")
    private GeneralTariffPlan generalTariffPlan;

    public Characteristic() {}

    public Characteristic(long id, String type, String generalInfo, String description) {
        this.id = id;
        this.type = type;
        this.generalInfo = generalInfo;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGeneralInfo() {
        return generalInfo;
    }

    public void setGeneralInfo(String generalInfo) {
        this.generalInfo = generalInfo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
