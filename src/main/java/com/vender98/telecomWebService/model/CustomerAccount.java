package com.vender98.telecomWebService.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CustomerAccount {
    @Id
    private long lineNumber;
    private String name;
    private int balance;
    private String token;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(long lineNumber) {
        this.lineNumber = lineNumber;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }
}
