package com.vender98.telecomWebService.constants;

public class Constants {
    public static final String BALANCE_KEY = "balance";
    public static final String HAS_AVAILABLE_TARIFF_PLANS_FLAG_KEY = "hasAvailableTariffPlans";
    public static final String HAS_AVAILABLE_SPENDING_ITEM_HISTORY_FLAG_KEY = "hasAvailableSpendingItemHistory";
    public static final String CUSTOMER_ACCOUNT_PATH = "customerAccount/";
    public static final String AUTHORIZATION_HEADER_KEY = "Authorization";
    public static final String JWT_LINE_NUMBER_KEY = "line_number";
    public static final String JWT_EXPIRATION_DATE_KEY = "exp";
    public static final String JWT_TOKEN_CREATION_DATE = "token_create_date";

    public static final int DEFAULT_PLANS_RANGE_SIZE = 3;
    public static final int DEFAULT_SPENDING_ITEMS_RANGE_SIZE = 9;
    public static final int DEFAULT_RANGES_COUNT = 3;
    public static final int DEFAULT_SMS_CODE_EXPIRATION_TIME = 30000;
    public static final int SMS_VERIFICATION_COUNTER_MAX_VALUE = 3;
    public static final int TOKEN_SESSION_STORAGE_LIFETIME = 300000;

    //HTTP response messages
    public static final String SMS_CODE_INVALID_MESSAGE = "SMS_CODE_INVALID";
    public static final String PHONE_NUMBER_INVALID_MESSAGE = "PHONE_NUMBER_INVALID";
    public static final String TOO_OFTEN_MESSAGE = "TOO_OFTEN";
    public static final String TOKEN_NOT_PROVIDED_MESSAGE = "TOKEN_NOT_PROVIDED";
    public static final String INCORRECT_LINE_NUMBER_FORMAT_MESSAGE = "INCORRECT_LINE_NUMBER_FORMAT";
    public static final String TOKEN_CORRUPTED_MESSAGE = "TOKEN_CORRUPTED";
    public static final String TOKEN_INVALID_MESSAGE = "TOKEN_INVALID";
    public static final String TOKEN_EXPIRED_MESSAGE = "TOKEN_EXPIRED";
    public static final String USER_NOT_FOUND_MESSAGE = "USER_WITH_SPECIFIED_TOKEN_NOT_FOUND";
    public static final String PERMISSION_DENIED_MESSAGE = "PERMISSION_DENIED";
}
