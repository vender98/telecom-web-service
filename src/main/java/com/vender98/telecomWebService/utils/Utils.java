package com.vender98.telecomWebService.utils;

public class Utils {
    public static int parsePrice(String priceString) {
        return Integer.valueOf(priceString.replaceAll("\\D+",""));
    }
}
