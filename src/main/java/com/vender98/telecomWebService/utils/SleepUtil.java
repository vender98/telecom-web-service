package com.vender98.telecomWebService.utils;

public class SleepUtil {

    private static final int TIMEOUT = 100;

    public static void sleep() {
        sleep(TIMEOUT);
    }

    public static void sleep(int timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
