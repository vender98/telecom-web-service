package com.vender98.telecomWebService.services;

import com.vender98.telecomWebService.model.CustomerAccount;
import com.vender98.telecomWebService.model.CustomerTariffPlan;

public interface CustomerAccountService {
    CustomerAccount getCustomerAccount(long lineNumber);

    CustomerTariffPlan getCustomerTariffPlan(long lineNumber);

    int recharge(long lineNumber, int amount);

    CustomerTariffPlan changeCustomerTariffPlan(long lineNumber, long tariffPlanId);
}
