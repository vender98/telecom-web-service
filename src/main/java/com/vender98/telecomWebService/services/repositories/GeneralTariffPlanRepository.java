package com.vender98.telecomWebService.services.repositories;

import com.vender98.telecomWebService.model.GeneralTariffPlan;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface GeneralTariffPlanRepository extends CrudRepository<GeneralTariffPlan, Long> {

    @Query(value = "SELECT * FROM general_tariff_plan WHERE id  <> :id", nativeQuery = true)
    List<GeneralTariffPlan> findAvailableTariffPlansWithOffset(long id, Pageable page);
}
