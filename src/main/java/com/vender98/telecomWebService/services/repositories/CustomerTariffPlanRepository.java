package com.vender98.telecomWebService.services.repositories;

import com.vender98.telecomWebService.model.CustomerTariffPlan;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CustomerTariffPlanRepository extends CrudRepository<CustomerTariffPlan, Long> {

    @Query(value = "SELECT general_tariff_plan_id FROM customer_tariff_plan WHERE line_number = :lineNumber", nativeQuery = true)
    Optional<Long> getGeneralTariffPlanIdByLineNumber(long lineNumber);
}
