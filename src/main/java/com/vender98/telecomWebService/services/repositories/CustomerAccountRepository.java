package com.vender98.telecomWebService.services.repositories;

import com.vender98.telecomWebService.model.CustomerAccount;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface CustomerAccountRepository extends CrudRepository<CustomerAccount, Long> {
    boolean existsByToken(String token);

    @Override
    boolean existsById(Long lineNumber);

    @Modifying
    @Transactional
    @Query(value = "UPDATE customer_account SET token = :token WHERE line_number = :lineNumber", nativeQuery = true)
    void setToken(long lineNumber, String token);

    @Modifying
    @Transactional
    @Query(value = "UPDATE customer_account SET token = null WHERE token = :token", nativeQuery = true)
    void removeToken(String token);

    Optional<CustomerAccount> findByToken(String token);

    @Query(value = "SELECT balance FROM customer_account WHERE line_number = :lineNumber", nativeQuery = true)
    Optional<Integer> getBalanceByLineNumber(long lineNumber);

    @Modifying
    @Transactional
    @Query(value = "UPDATE customer_account SET balance = :balance WHERE line_number = :lineNumber", nativeQuery = true)
    void setBalanceByLineNumber(long lineNumber, int balance);
}
