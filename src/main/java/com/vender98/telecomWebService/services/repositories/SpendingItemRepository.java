package com.vender98.telecomWebService.services.repositories;

import com.vender98.telecomWebService.model.SpendingItem;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.List;

public interface SpendingItemRepository extends CrudRepository<SpendingItem, Long> {

    @Query(value = "SELECT name, amount, item_type_id FROM spending_item WHERE line_number = :lineNumber AND " +
            "date BETWEEN :from AND :to", nativeQuery = true)
    List<Object[]> getSpendingHistory(long lineNumber, Timestamp from, Timestamp to);

    @Query(value = "SELECT amount, details, date, description FROM spending_item WHERE line_number = :lineNumber AND " +
            "item_type_id = :itemTypeId AND date BETWEEN :from AND :to", nativeQuery = true)
    List<Object[]> getSpendingItemHistory(long lineNumber, long itemTypeId, Timestamp from, Timestamp to, Pageable page);
}
