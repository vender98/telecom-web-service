package com.vender98.telecomWebService.services.implementations;

import com.vender98.telecomWebService.constants.Constants;
import com.vender98.telecomWebService.model.SpendingItem;
import com.vender98.telecomWebService.services.SpendingHistoryService;
import com.vender98.telecomWebService.services.repositories.SpendingItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service("SpendingHistoryServiceImpl")
public class SpendingHistoryServiceImpl implements SpendingHistoryService {

    @Autowired
    private SpendingItemRepository spendingItemRepository;

    @Override
    public List<SpendingItem> getSpendingHistory(long lineNumber, Timestamp from, Timestamp to) {
        List<Object[]> objects = spendingItemRepository.getSpendingHistory(lineNumber, from, to);
        List<SpendingItem> spendingItems = new ArrayList<>();
        objects.forEach(object -> {
            spendingItems.add(new SpendingItem((String) object[0], (Integer) object[1], ((BigInteger) object[2]).longValue()));
        });
        Map<String, List<SpendingItem>> nameToItemsMap = StreamSupport.stream(spendingItems.spliterator(), false)
                .collect(Collectors.groupingBy(SpendingItem::getName));

        List<SpendingItem> result = new ArrayList<>();

        nameToItemsMap.entrySet().forEach(entry -> {
            String name = entry.getKey();
            int amount = nameToItemsMap.get(name)
                    .stream()
                    .map(SpendingItem::getAmount)
                    .reduce(Integer::sum)
                    .orElse(0);
            result.add(new SpendingItem(name, amount, nameToItemsMap.get(name).get(0).getItemTypeId()));
        });

        return result;
    }

    @Override
    public List<SpendingItem> getItemSpendingHistory(long lineNumber, long itemTypeId, Timestamp from, Timestamp to, int range) {
        return getItemSpendingHistory(lineNumber, itemTypeId, from, to,
                PageRequest.of(range - 1, Constants.DEFAULT_SPENDING_ITEMS_RANGE_SIZE));
    }

    @Override
    public boolean hasAvailableSpendingItemHistory(long lineNumber, long itemTypeId, Timestamp from, Timestamp to, int range) {
        List<SpendingItem> spendingItems = getItemSpendingHistory(lineNumber, itemTypeId, from, to,
                PageRequest.of((range - 1) * Constants.DEFAULT_SPENDING_ITEMS_RANGE_SIZE, 1));
        return spendingItems.size() > 0;
    }

    private List<SpendingItem> getItemSpendingHistory(long lineNumber, long itemTypeId, Timestamp from, Timestamp to, PageRequest pageRequest) {
        List<Object[]> objects = spendingItemRepository.getSpendingItemHistory(lineNumber, itemTypeId, from, to, pageRequest);
        List<SpendingItem> spendingItems = new ArrayList<>();
        objects.forEach(object -> {
            spendingItems.add(new SpendingItem((Integer) object[0], (String) object[1], (Timestamp) object[2], (String) object[3]));
        });
        return spendingItems;
    }
}
