package com.vender98.telecomWebService.services.implementations;

import com.vender98.telecomWebService.constants.Constants;
import com.vender98.telecomWebService.model.GeneralTariffPlan;
import com.vender98.telecomWebService.services.TariffPlanService;
import com.vender98.telecomWebService.services.repositories.CustomerTariffPlanRepository;
import com.vender98.telecomWebService.services.repositories.GeneralTariffPlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service(value = "TariffPlanServiceImpl")
public class TariffPlanServiceImpl implements TariffPlanService {

    @Autowired
    GeneralTariffPlanRepository generalTariffPlanRepository;

    @Autowired
    CustomerTariffPlanRepository customerTariffPlanRepository;

    @Override
    public GeneralTariffPlan getActiveTariffPlan(long lineNumber) {
        Optional<Long> generalTariffPlanId = customerTariffPlanRepository.getGeneralTariffPlanIdByLineNumber(lineNumber);
        Optional<GeneralTariffPlan> generalTariffPlan = generalTariffPlanRepository.findById(generalTariffPlanId.get());
        return generalTariffPlan.orElse(null);
    }

    @Override
    public List<GeneralTariffPlan> getAvailableTariffPlans(long lineNumber, int range) {
        Optional<Long> generalTariffPlanId = customerTariffPlanRepository.getGeneralTariffPlanIdByLineNumber(lineNumber);
        List<GeneralTariffPlan> plans = generalTariffPlanRepository.findAvailableTariffPlansWithOffset(generalTariffPlanId.get(),
                PageRequest.of(range - 1, Constants.DEFAULT_PLANS_RANGE_SIZE));
        return plans;
    }

    @Override
    public boolean hasAvailableTariffPlans(long lineNumber, int range) {
        Optional<Long> generalTariffPlanId = customerTariffPlanRepository.getGeneralTariffPlanIdByLineNumber(lineNumber);
        List<GeneralTariffPlan> plans = generalTariffPlanRepository.findAvailableTariffPlansWithOffset(generalTariffPlanId.get(),
                PageRequest.of((range - 1) * Constants.DEFAULT_PLANS_RANGE_SIZE, 1));
        return plans.size() > 0;
    }
}
