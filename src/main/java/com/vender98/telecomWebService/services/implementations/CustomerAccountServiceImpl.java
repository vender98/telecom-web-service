package com.vender98.telecomWebService.services.implementations;

import com.vender98.telecomWebService.model.CustomerAccount;
import com.vender98.telecomWebService.model.CustomerTariffPlan;
import com.vender98.telecomWebService.model.GeneralTariffPlan;
import com.vender98.telecomWebService.model.Package;
import com.vender98.telecomWebService.services.CustomerAccountService;
import com.vender98.telecomWebService.services.repositories.CustomerAccountRepository;
import com.vender98.telecomWebService.services.repositories.CustomerTariffPlanRepository;
import com.vender98.telecomWebService.services.repositories.GeneralTariffPlanRepository;
import com.vender98.telecomWebService.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service(value="CustomerAccountServiceImpl")
public class CustomerAccountServiceImpl implements CustomerAccountService {

    @Autowired
    private CustomerAccountRepository customerAccountRepository;

    @Autowired
    private CustomerTariffPlanRepository customerTariffPlanRepository;

    @Autowired
    private GeneralTariffPlanRepository generalTariffPlanRepository;

    @Override
    public CustomerAccount getCustomerAccount(long lineNumber) {
        Optional<CustomerAccount> customerAccountWrapper = customerAccountRepository.findById(lineNumber);
        return customerAccountWrapper.orElse(null);
    }

    @Override
    public CustomerTariffPlan getCustomerTariffPlan(long lineNumber) {
        Optional<CustomerTariffPlan> customerTariffPlanWrapper = customerTariffPlanRepository.findById(lineNumber);
        return customerTariffPlanWrapper.orElse(null);
    }

    @Override
    public int recharge(long lineNumber, int amount) {
        Optional<Integer> customerAccountWrapper = customerAccountRepository.getBalanceByLineNumber(lineNumber);
        int oldBalance = customerAccountWrapper.get();
        int newNalance = oldBalance + amount;
        customerAccountRepository.setBalanceByLineNumber(lineNumber, newNalance);
        return newNalance;
    }

    @Override
    public CustomerTariffPlan changeCustomerTariffPlan(long lineNumber, long tariffPlanId) {
        GeneralTariffPlan generalTariffPlan = generalTariffPlanRepository.findById(tariffPlanId).get();

        CustomerTariffPlan customerTariffPlan = new CustomerTariffPlan();
        customerTariffPlan.setName(generalTariffPlan.getName());
        customerTariffPlan.setConnectionDate(new Timestamp(new Date().getTime()));
        customerTariffPlan.setLineNumber(lineNumber);
        customerTariffPlan.setGeneralTariffPlan(generalTariffPlan);

        Set<Package> packages = new HashSet<>();
        packages.add(new Package("Megabytes", generalTariffPlan.getMegabytes(),
                generalTariffPlan.getMegabytes(), customerTariffPlan));
        packages.add(new Package("Minutes", generalTariffPlan.getMinutes(),
                generalTariffPlan.getMinutes(), customerTariffPlan));
        packages.add(new Package("Sms", generalTariffPlan.getSms(),
                generalTariffPlan.getSms(), customerTariffPlan));

        customerTariffPlan.setPackages(packages);

        int oldBalance = customerAccountRepository.getBalanceByLineNumber(lineNumber).get();
        int newBalance = oldBalance - Utils.parsePrice(generalTariffPlan.getPrice());

        if (newBalance < 0) {
            return null;
        }

        customerAccountRepository.setBalanceByLineNumber(lineNumber, newBalance);
        customerTariffPlanRepository.save(customerTariffPlan);
        return customerTariffPlan;
    }
}
