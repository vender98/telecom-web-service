package com.vender98.telecomWebService.services.stubs;

import com.vender98.telecomWebService.model.CustomerAccount;
import com.vender98.telecomWebService.model.CustomerTariffPlan;
import com.vender98.telecomWebService.model.Package;
import com.vender98.telecomWebService.services.CustomerAccountService;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Random;

@Service(value="CustomerAccountServiceStub")
public class CustomerAccountServiceStub implements CustomerAccountService {
    @Override
    public CustomerAccount getCustomerAccount(long lineNumber) {
        CustomerAccount customerAccount = new CustomerAccount();
        customerAccount.setName("Anakin Skywalker");
        customerAccount.setLineNumber(79123456789L);
        customerAccount.setBalance(Math.abs(new Random().nextInt() % 100));
        return customerAccount;
    }

    @Override
    public CustomerTariffPlan getCustomerTariffPlan(long lineNumber) {

        long tariffPlanId  = (long) Math.abs(new Random().nextInt() % 100);

        Calendar connectionDate = Calendar.getInstance();
        connectionDate.add(Calendar.DAY_OF_MONTH, -15);

        CustomerTariffPlan customerTariffPlan = new CustomerTariffPlan();
        customerTariffPlan.setLineNumber(lineNumber);
        customerTariffPlan.setConnectionDate(new Timestamp(connectionDate.getTimeInMillis()));
        customerTariffPlan.setName("Лучший");
        customerTariffPlan.setPackages(new HashSet<>(Arrays.asList(
                new Package("Megabytes", 1000, 500, null),
                new Package("Minutes",300, 200, null),
                new Package("Sms", 300, 250, null)
        )));

        return customerTariffPlan;
    }

    @Override
    public int recharge(long lineNumber, int amount) {
        return amount;
    }

    @Override
    public CustomerTariffPlan changeCustomerTariffPlan(long lineNumber, long tariffPlanId) {
        Calendar connectionDate = Calendar.getInstance();

        CustomerTariffPlan customerTariffPlan = new CustomerTariffPlan();
        customerTariffPlan.setLineNumber(lineNumber);
        customerTariffPlan.setConnectionDate(new Timestamp(connectionDate.getTimeInMillis()));
        customerTariffPlan.setName("Новый");
        customerTariffPlan.setPackages(new HashSet<>(Arrays.asList(
                new Package("Megabytes", 1000, 1000, null),
                new Package("Minutes",300, 300, null),
                new Package("Sms", 300, 300, null)
        )));

        return customerTariffPlan;
    }
}
