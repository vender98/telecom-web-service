package com.vender98.telecomWebService.services.stubs;

import com.vender98.telecomWebService.constants.Constants;
import com.vender98.telecomWebService.model.Characteristic;
import com.vender98.telecomWebService.model.GeneralTariffPlan;
import com.vender98.telecomWebService.services.TariffPlanService;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Service(value="TariffPlanServiceStub")
public class TariffPlanServiceStub implements TariffPlanService {
    @Override
    public GeneralTariffPlan getActiveTariffPlan(long lineNumber) {
        List<Characteristic> characteristics = generateCharacteristics();

        return new GeneralTariffPlan(1, "Лучший", "370₽/месяц",
                new HashSet<>(characteristics), 0, 0, 0);
    }

    @Override
    public List<GeneralTariffPlan> getAvailableTariffPlans(long lineNumber, int range) {
        List<Characteristic> characteristics = generateCharacteristics();

        GeneralTariffPlan first = new GeneralTariffPlan(1, "Худший", "100₽/месяц",
                new HashSet<>(characteristics), 0, 0, 0);

        GeneralTariffPlan second = new GeneralTariffPlan(2, "Средний", "200₽/месяц",
                new HashSet<>(characteristics), 0, 0, 0);

        GeneralTariffPlan third = new GeneralTariffPlan(3, "Хороший", "300₽/месяц",
                new HashSet<>(characteristics), 0, 0, 0);

        return Arrays.asList(first, second, third);
    }



    private List<Characteristic> generateCharacteristics() {
        return Arrays.asList(
                new Characteristic(1L,
                        "SMS",
                        "100\nСМС",
                        "абонентам домашнего региона"),
                new Characteristic(1L,
                        "internet",
                        "10\nГБ",
                        "интернет"),
                new Characteristic(1L,
                        "minutes",
                        "500\nминут",
                        "абонентам домашнего региона"),
                new Characteristic(1L,
                        "entertainment",
                        "∞",
                        "безлимитный интернет на набор сервисов" +
                                "(YoTube, Facebook, Instagram и др.)")
        );
    }

    @Override
    public boolean hasAvailableTariffPlans(long lineNumber, int range) {
        return range <= Constants.DEFAULT_RANGES_COUNT;
    }
}
