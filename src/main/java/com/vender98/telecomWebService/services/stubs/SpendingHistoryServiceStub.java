package com.vender98.telecomWebService.services.stubs;

import com.vender98.telecomWebService.model.SpendingItem;
import com.vender98.telecomWebService.services.SpendingHistoryService;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service(value="SpendingHistoryServiceStub")
public class SpendingHistoryServiceStub implements SpendingHistoryService {
    @Override
    public List<SpendingItem> getSpendingHistory(long lineNumber, Timestamp from, Timestamp to) {

//        return Arrays.asList(
//                new SpendingItem(1, "Абонентская плата", 360),
//                new SpendingItem(2, "СМС", 50),
//                new SpendingItem(3, "Интернет", 40),
//                new SpendingItem(4, "Звонки", 30),
//                new SpendingItem(5, "Развлечения", 20)
//        );
        return null;
    }

    @Override
    public List<SpendingItem> getItemSpendingHistory(long lineNumber, long itemId, Timestamp from, Timestamp to, int range) {
        return null;
    }

//    @Override
//    public List<SpendingItemDetailed> getItemSpendingHistory(long lineNumber, long itemId,
//                                                             Timestamp from, Timestamp to, int range) {
//        Calendar calendar = Calendar.getInstance();
//        Timestamp timestamp = new Timestamp(0);
//
//        List<SpendingItemDetailed> spendingItemDetailedList = new ArrayList<>();
//        calendar.add(Calendar.DAY_OF_MONTH, -(range - 1) * Constants.DEFAULT_PLANS_RANGE_SIZE);
//        for (int i = 0; i < Constants.DEFAULT_PLANS_RANGE_SIZE; ++i) {
//            calendar.add(Calendar.DAY_OF_MONTH, -1);
//            for (int j = 0; j < 3; ++j) {
//                timestamp.setTime(calendar.getTimeInMillis());
//
//                spendingItemDetailedList.add(new SpendingItemDetailed(1,
//                        "+7 (981) 890-06-26",
//                        2,
//                        new Timestamp(timestamp.getTime()),
//                        "0:02 мин"));
//            }
//        }
//
//        return spendingItemDetailedList;
//    }

    @Override
    public boolean hasAvailableSpendingItemHistory(long lineNumber, long itemId, Timestamp from, Timestamp to, int range) {
        return false;
    }

//    @Override
//    public boolean hasAvailableSpendingItemHistory(long lineNumber, int range) {
//        return range <= Constants.DEFAULT_RANGES_COUNT;
//    }
}
