package com.vender98.telecomWebService.services;

import com.vender98.telecomWebService.model.SpendingItem;

import java.sql.Timestamp;
import java.util.List;

public interface SpendingHistoryService {
    List<SpendingItem> getSpendingHistory(long lineNumber, Timestamp from, Timestamp to);

    List<SpendingItem> getItemSpendingHistory(long lineNumber, long itemId, Timestamp from, Timestamp to, int range);

    boolean hasAvailableSpendingItemHistory(long lineNumber, long itemId, Timestamp from, Timestamp to, int range);
}
