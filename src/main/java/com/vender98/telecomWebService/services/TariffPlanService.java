package com.vender98.telecomWebService.services;

import com.vender98.telecomWebService.model.GeneralTariffPlan;

import java.util.List;

public interface TariffPlanService {
    GeneralTariffPlan getActiveTariffPlan(long lineNumber);

    List<GeneralTariffPlan> getAvailableTariffPlans(long lineNumber, int range);

    boolean hasAvailableTariffPlans(long lineNumber, int range);
}
