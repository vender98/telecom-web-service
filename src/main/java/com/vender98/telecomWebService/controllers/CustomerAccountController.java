package com.vender98.telecomWebService.controllers;

import com.vender98.telecomWebService.constants.Constants;
import com.vender98.telecomWebService.model.CustomerAccount;
import com.vender98.telecomWebService.model.CustomerTariffPlan;
import com.vender98.telecomWebService.model.GeneralTariffPlan;
import com.vender98.telecomWebService.model.SpendingItem;
import com.vender98.telecomWebService.services.CustomerAccountService;
import com.vender98.telecomWebService.services.SpendingHistoryService;
import com.vender98.telecomWebService.services.TariffPlanService;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/customerAccount")
public class CustomerAccountController {

    @Autowired
    @Qualifier("CustomerAccountServiceEnvironmentProvider")
    private CustomerAccountService customerAccountService;

    @Autowired
    @Qualifier("TariffPlanServiceEnvironmentProvider")
    private TariffPlanService tariffPlanService;

    @Autowired
    @Qualifier("SpendingHistoryServiceEnvironmentProvider")
    private SpendingHistoryService spendingHistoryService;

    @GetMapping("/{lineNumber}")
    public CustomerAccount getCustomerAccount(@PathVariable long lineNumber) {
        return customerAccountService.getCustomerAccount(lineNumber);
    }

    @GetMapping("/{lineNumber}/tariffPlan")
    public CustomerTariffPlan getCustomerTariffPlan(@PathVariable long lineNumber) {
        return customerAccountService.getCustomerTariffPlan(lineNumber);
    }

    @PostMapping("/{lineNumber}/recharge")
    public Map rechargeCustomerAccount(@PathVariable long lineNumber,
                                       @RequestParam(name = "amount") int amount) {
        Map<String, Object> params = new HashMap<>();

        int result = customerAccountService.recharge(lineNumber, amount);

        params.put(Constants.BALANCE_KEY, result);
        return params;
    }

    @GetMapping("/{lineNumber}/tariffPlanDetailed")
    public GeneralTariffPlan getGeneralTariffPlan(@PathVariable long lineNumber) {
        return tariffPlanService.getActiveTariffPlan(lineNumber);
    }

    @GetMapping("/{lineNumber}/tariffPlansAvailable")
    public List<GeneralTariffPlan> getAvailableTariffPlans(@PathVariable long lineNumber,
                                                           @RequestParam(name = "range") int range) {
        return tariffPlanService.getAvailableTariffPlans(lineNumber, range);
    }

    @PostMapping("/{lineNumber}/changeTariffPlan")
    public CustomerTariffPlan changeTariffPlan(@PathVariable long lineNumber,
                                               @RequestParam(name = "tariffPlanId") long tariffPlanId,
                                               HttpServletResponse response) {
        CustomerTariffPlan customerTariffPlan = customerAccountService.changeCustomerTariffPlan(lineNumber, tariffPlanId);
        if (customerTariffPlan == null) {
            response.setStatus(HttpStatus.SC_BAD_REQUEST);
            return null;
        } else {
            return customerTariffPlan;
        }
    }

    @GetMapping("/{lineNumber}/hasAvailableTariffPlans")
    public Map hasAvailableTariffPlans(@PathVariable long lineNumber,
                                      @RequestParam(name = "range") int range) {
        Map<String, Object> params = new HashMap<>();
        params.put(Constants.HAS_AVAILABLE_TARIFF_PLANS_FLAG_KEY,
                tariffPlanService.hasAvailableTariffPlans(lineNumber, range));

        return params;
    }

    @GetMapping("/{lineNumber}/spendingHistory")
    public List<SpendingItem> getSpendingHistory(@PathVariable long lineNumber,
                                                 @RequestParam("from") long from,
                                                 @RequestParam("to") long to) {
        return spendingHistoryService.getSpendingHistory(lineNumber, new Timestamp(from), new Timestamp(to));
    }

    @GetMapping("/{lineNumber}/spendingHistory/{spendingItemId}")
    public List<SpendingItem> getItemSpendingHistory(@PathVariable long lineNumber,
                                                             @PathVariable long spendingItemId,
                                                             @RequestParam("from") long from,
                                                             @RequestParam("to") long to,
                                                             @RequestParam("range") int range) {
        return spendingHistoryService.getItemSpendingHistory(lineNumber,
                spendingItemId, new Timestamp(from), new Timestamp(to), range);
    }

    @GetMapping("/{lineNumber}/hasAvailableSpendingItemHistory/{spendingItemId}")
    public Map hasAvailableSpendingItemHistory(@PathVariable long lineNumber,
                                               @PathVariable long spendingItemId,
                                               @RequestParam("from") long from,
                                               @RequestParam("to") long to,
                                               @RequestParam("range") int range) {
        Map<String, Object> params = new HashMap<>();
        params.put(Constants.HAS_AVAILABLE_SPENDING_ITEM_HISTORY_FLAG_KEY,
                spendingHistoryService.hasAvailableSpendingItemHistory(lineNumber, spendingItemId,
                        new Timestamp(from), new Timestamp(to), range));

        return params;
    }

}
