package com.vender98.telecomWebService.config;

import com.vender98.telecomWebService.authentication.api.SendMessageAPI;
import com.vender98.telecomWebService.authentication.services.AuthenticationService;
import com.vender98.telecomWebService.services.CustomerAccountService;
import com.vender98.telecomWebService.services.SpendingHistoryService;
import com.vender98.telecomWebService.services.TariffPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EnvironmentConfig {

    @Autowired
    private ApplicationContext context;

    @Bean
    public CustomerAccountService CustomerAccountServiceEnvironmentProvider(@Value("${application.environment}") String qualifier) {
        return (CustomerAccountService) context.getBean("CustomerAccountService" + qualifier);
    }

    @Bean
    public TariffPlanService TariffPlanServiceEnvironmentProvider(@Value("${application.environment}") String qualifier) {
        return (TariffPlanService) context.getBean("TariffPlanService" + qualifier);
    }

    @Bean
    public SpendingHistoryService SpendingHistoryServiceEnvironmentProvider(@Value("${application.environment}") String qualifier) {
        return (SpendingHistoryService) context.getBean("SpendingHistoryService" + qualifier);
    }

    @Bean
    public AuthenticationService AuthenticationServiceEnvironmentProvider(@Value("${sms.code.environment}") String qualifier) {
        return (AuthenticationService) context.getBean("AuthenticationService" + qualifier);
    }

    @Bean
    public SendMessageAPI SendMessageAPIProvider(@Value("${send.message.api}") String qualifier) {
        return (SendMessageAPI) context.getBean(qualifier + "API");
    }
}
