package com.vender98.telecomWebService.authentication.managers;

import com.vender98.telecomWebService.authentication.model.TokenAuthentication;
import com.vender98.telecomWebService.constants.Constants;
import com.vender98.telecomWebService.model.CustomerAccount;
import com.vender98.telecomWebService.services.repositories.CustomerAccountRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.impl.DefaultClaims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class TokenAuthenticationManager implements AuthenticationManager {

    @Autowired
    private CustomerAccountRepository customerAccountRepository;

    @Value("${jwt.key}")
    String jwtKey;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            TokenAuthentication readyTokenAuthentication = processAuthentication((TokenAuthentication) authentication);
            return readyTokenAuthentication;
        } catch (Exception ex) {
            if(ex instanceof AuthenticationServiceException)
                throw ex;
            return null;
        }
    }

    private TokenAuthentication processAuthentication(TokenAuthentication authentication) throws AuthenticationException {
        String token = authentication.getToken();
        long lineNumber = authentication.getLineNumber();
        DefaultClaims claims;
        try {
            claims = (DefaultClaims) Jwts.parser().setSigningKey(jwtKey).parse(token).getBody();
        } catch (Exception ex) {
            throw new AuthenticationServiceException(Constants.TOKEN_CORRUPTED_MESSAGE);
        }
        if (claims.get(Constants.JWT_EXPIRATION_DATE_KEY, Long.class) == null)
            throw new AuthenticationServiceException(Constants.TOKEN_INVALID_MESSAGE);
        Date expiredDate = new Date(claims.get(Constants.JWT_EXPIRATION_DATE_KEY, Long.class));
        if (expiredDate.before(new Date()))
            throw new AuthenticationServiceException(Constants.TOKEN_EXPIRED_MESSAGE);
        Optional<CustomerAccount> user = customerAccountRepository.findByToken(token);
        if (!user.isPresent())
            throw new AuthenticationServiceException(Constants.USER_NOT_FOUND_MESSAGE);
        if (lineNumber != (long) claims.get(Constants.JWT_LINE_NUMBER_KEY))
            throw new AuthenticationServiceException(Constants.PERMISSION_DENIED_MESSAGE);
        return new TokenAuthentication(authentication.getToken(), lineNumber, true);
    }
}