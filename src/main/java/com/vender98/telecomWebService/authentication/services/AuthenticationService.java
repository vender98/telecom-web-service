package com.vender98.telecomWebService.authentication.services;

public interface AuthenticationService {

    boolean customerIsRegistered(long lineNumber);

    String generateToken(long lineNumber);

    boolean isTokenValid(String token);

    String generateOneTimeSmsCode();

    void sendSmsCode(String code);

    boolean isResendCodeAvailable();

    boolean isVerificationSmsAvailable();

    boolean verifySmsCode(String code);

    void saveCustomerToken();

    boolean customerHasToken(String token);

    void logout(String token);
}
