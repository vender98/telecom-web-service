package com.vender98.telecomWebService.authentication.services.stubs;

import com.vender98.telecomWebService.authentication.services.AuthenticationServiceAbstract;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

@Service(value = "AuthenticationServiceStub")
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class AuthenticationServiceStub extends AuthenticationServiceAbstract {

    @Override
    public void sendSmsCode(String code) {
        logger.info("SMS code: " + sessionSmsCode);
    }
}