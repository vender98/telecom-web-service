package com.vender98.telecomWebService.authentication.services;

import com.vender98.telecomWebService.constants.Constants;
import com.vender98.telecomWebService.services.repositories.CustomerAccountRepository;
import com.vender98.telecomWebService.utils.SleepUtil;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public abstract class AuthenticationServiceAbstract implements AuthenticationService {

    @Autowired
    protected CustomerAccountRepository customerAccountRepository;

    protected Logger logger = LoggerFactory.getLogger(AuthenticationServiceAbstract.class);

    protected String token;
    protected long lineNumber;
    protected String sessionSmsCode;
    protected boolean canResendSms = true;
    protected int smsVerificationCounter = 0;

    @Value("${jwt.key}")
    String jwtKey;

    @Override
    public boolean customerIsRegistered(long lineNumber) {
        return customerAccountRepository.existsById(lineNumber);
    }

    @Override
    public String generateToken(long lineNumber) {
        Map<String, Object> tokenData = new HashMap<>();

        tokenData.put(Constants.JWT_LINE_NUMBER_KEY, lineNumber);
        tokenData.put(Constants.JWT_TOKEN_CREATION_DATE, new Date().getTime());
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, 100);
        tokenData.put(Constants.JWT_EXPIRATION_DATE_KEY, calendar.getTimeInMillis());
        JwtBuilder jwtBuilder = Jwts.builder().setClaims(tokenData);
        this.token = jwtBuilder.signWith(SignatureAlgorithm.HS256, jwtKey).compact();
        this.lineNumber = lineNumber;

        return this.token;
    }

    @Override
    public boolean isTokenValid(String token) {
        return token.equals(this.token);
    }

    @Override
    public String generateOneTimeSmsCode() {
        this.sessionSmsCode = RandomStringUtils.random(6, false, true);
        String requestCode = this.sessionSmsCode;
        this.canResendSms = false;
        new Thread(() -> {
            SleepUtil.sleep(Constants.DEFAULT_SMS_CODE_EXPIRATION_TIME);
            if (requestCode.equals(this.sessionSmsCode)){
                logger.info("SMS code " + sessionSmsCode + " is expired");
                this.sessionSmsCode = null;
                this.canResendSms = true;
                this.smsVerificationCounter = 0;
            }
        }).start();
        return this.sessionSmsCode;
    }

    @Override
    public boolean isResendCodeAvailable() {
        return this.canResendSms;
    }

    @Override
    public boolean isVerificationSmsAvailable() {
        return this.smsVerificationCounter < Constants.SMS_VERIFICATION_COUNTER_MAX_VALUE;
    }

    @Override
    public boolean verifySmsCode(String code) {
        if (code.equals(this.sessionSmsCode)) {
            logger.info("SMS code " + sessionSmsCode + " is successfully verified");
            this.sessionSmsCode = null;
            this.smsVerificationCounter = 0;
            return true;
        } else {
            this.smsVerificationCounter++;
            logger.info("SMS verification counter = " + this.smsVerificationCounter);
            return false;
        }
    }

    @Override
    public void saveCustomerToken() {
        customerAccountRepository.setToken(this.lineNumber, this.token);
        this.token = null;
        this.lineNumber = 0;
    }

    @Override
    public boolean customerHasToken(String token) {
        return customerAccountRepository.existsByToken(token);
    }

    @Override
    public void logout(String token) {
        customerAccountRepository.removeToken(token);
    }
}
