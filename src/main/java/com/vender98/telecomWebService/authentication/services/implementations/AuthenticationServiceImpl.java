package com.vender98.telecomWebService.authentication.services.implementations;

import com.vender98.telecomWebService.authentication.api.SendMessageAPI;
import com.vender98.telecomWebService.authentication.services.AuthenticationServiceAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

@Service(value = "AuthenticationServiceImpl")
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class AuthenticationServiceImpl extends AuthenticationServiceAbstract {

    @Autowired
    @Qualifier("SendMessageAPIProvider")
    SendMessageAPI sendMessageAPI;

    @Override
    public void sendSmsCode(String code) {
        try {
            sendMessageAPI.sendMessage(lineNumber, code);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
