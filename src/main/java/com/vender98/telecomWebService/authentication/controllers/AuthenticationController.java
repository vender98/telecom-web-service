package com.vender98.telecomWebService.authentication.controllers;

import com.vender98.telecomWebService.authentication.services.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/authentication")
public class AuthenticationController {

    @Autowired
    @Qualifier("AuthenticationServiceEnvironmentProvider")
    private AuthenticationService authenticationService;

    @PostMapping("generateCode")
    public String getTokenAndGenerateSmsCode(@RequestParam("lineNumber") long lineNumber,
                                  HttpServletResponse response) {
        if (authenticationService.customerIsRegistered(lineNumber)) {
            String token = authenticationService.generateToken(lineNumber);
            authenticationService.sendSmsCode(authenticationService.generateOneTimeSmsCode());
            return token;
        } else {
            response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
            return null;
        }
    }

    @PutMapping("{token}/resendCode")
    public void resendSmsCode(@PathVariable String token,
                              HttpServletResponse response) {
        if (!authenticationService.isTokenValid(token)) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
        } else if (!authenticationService.isResendCodeAvailable()) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
        } else {
            authenticationService.sendSmsCode(authenticationService.generateOneTimeSmsCode());
            response.setStatus(HttpStatus.OK.value());
        }
    }

    @PutMapping("{token}/verifyCode")
    public void verifySmsCode(@PathVariable String token,
                                 @RequestParam("code") String code,
                                 HttpServletResponse response) {
        if (!authenticationService.isTokenValid(token)) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
        } else if (!authenticationService.isVerificationSmsAvailable()) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
        } else if (!authenticationService.verifySmsCode(code)) {
            response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
        } else {
            authenticationService.saveCustomerToken();
            response.setStatus(HttpStatus.OK.value());
        }
    }

    @DeleteMapping("{token}/logout")
    public void logout(@PathVariable String token,
                       HttpServletResponse response) {
        if (authenticationService.customerHasToken(token)) {
            authenticationService.logout(token);
        } else {
            response.setStatus(HttpStatus.NOT_FOUND.value());
        }
    }

    @GetMapping("{token}/isValid")
    public void isTokenValid(@PathVariable String token,
                             HttpServletResponse response) {
        if (!authenticationService.customerHasToken(token)) {
            response.setStatus(HttpStatus.FORBIDDEN.value());
        } else {
            response.setStatus(HttpStatus.OK.value());
        }
    }
}
