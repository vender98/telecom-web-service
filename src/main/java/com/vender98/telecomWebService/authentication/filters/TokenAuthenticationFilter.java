package com.vender98.telecomWebService.authentication.filters;

import com.vender98.telecomWebService.authentication.model.TokenAuthentication;
import com.vender98.telecomWebService.constants.Constants;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TokenAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    public TokenAuthenticationFilter() {
        super("/" + Constants.CUSTOMER_ACCOUNT_PATH + "**");
        setAuthenticationFailureHandler((request, response, authenticationException) -> {
            response.getOutputStream().print(authenticationException.getMessage());
            response.setStatus(HttpStatus.FORBIDDEN.value());
        });
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        SecurityContextHolder.getContext().setAuthentication(authResult);
        chain.doFilter(request, response);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        String token = request.getHeader(Constants.AUTHORIZATION_HEADER_KEY);
        if (token == null) {
            throw new AuthenticationServiceException(Constants.TOKEN_NOT_PROVIDED_MESSAGE);
        }
        TokenAuthentication tokenAuthentication;
        try {
            tokenAuthentication = new TokenAuthentication(token, extractLineNumber(request));
        } catch (NumberFormatException ex) {
            throw new AuthenticationServiceException(Constants.INCORRECT_LINE_NUMBER_FORMAT_MESSAGE);
        }
        Authentication authentication = getAuthenticationManager().authenticate(tokenAuthentication);
        return authentication;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
                         FilterChain chain) throws IOException, ServletException {
        super.doFilter(req, res, chain);
    }

    private long extractLineNumber(HttpServletRequest request) {
        String URI = request.getRequestURI();
        String substringAfter = StringUtils.substringAfter(URI, Constants.CUSTOMER_ACCOUNT_PATH);
        String lineNumber = substringAfter.substring(0, 11);
        return Long.valueOf(lineNumber);
    }
}