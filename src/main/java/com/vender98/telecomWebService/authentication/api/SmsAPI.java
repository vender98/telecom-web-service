package com.vender98.telecomWebService.authentication.api;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service(value = "SmsAPI")
public class SmsAPI extends SendMessageAPI {
    private static final String ENDPOINT = "https://sms.ru/sms/send";
    private static final String TO_KEY = "to";
    private static final String MSG_KEY = "msg";
    private static final String JSON_KEY = "json";
    private static final String API_ID_KEY = "api_id";

    private static final String API_ID_VALUE = "3FBE8A9A-B0ED-38C9-481B-794415ADCA9F";

    @Override
    protected String getEndpoint() {
        return ENDPOINT;
    }

    @Override
    protected List<NameValuePair> getParams(long id, String message) {
        List<NameValuePair> urlParameters = new ArrayList<>();
        urlParameters.add(new BasicNameValuePair(TO_KEY, String.valueOf(id)));
        urlParameters.add(new BasicNameValuePair(MSG_KEY, message));
        urlParameters.add(new BasicNameValuePair(JSON_KEY, "1"));
        urlParameters.add(new BasicNameValuePair(API_ID_KEY, API_ID_VALUE));
        return urlParameters;
    }
}
