package com.vender98.telecomWebService.authentication.api;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.util.List;

import static org.springframework.http.HttpHeaders.USER_AGENT;

public abstract class SendMessageAPI {
    public void sendMessage (long id, String message) throws IOException {
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(getEndpoint());

        post.setHeader("User-Agent", USER_AGENT);
        List<NameValuePair> urlParameters = getParams(id, message);
        post.setEntity(new UrlEncodedFormEntity(urlParameters));
        client.execute(post);
    }

    protected abstract String getEndpoint();

    protected abstract List<NameValuePair> getParams(long id, String message);
}
