package com.vender98.telecomWebService.authentication.api;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service(value = "TelegramAPI")
public class TelegramAPI extends SendMessageAPI {

    private static final String TELEGRAM_BOT_TOKEN = "888536114:AAE60KohMLCJLKvGh7ntOFjOJ-G04u29lVs";
    private static final String ENDPOINT = "https://api.telegram.org/bot" + TELEGRAM_BOT_TOKEN + "/sendMessage";
    private static final String CHAT_ID_KEY = "chat_id";
    private static final String CHAT_ID_VALUE = "389487222";
    private static final String TEXT_KEY = "text";

    @Override
    protected String getEndpoint() {
        return ENDPOINT;
    }

    @Override
    protected List<NameValuePair> getParams(long id, String message) {
        List<NameValuePair> urlParameters = new ArrayList<>();
        urlParameters.add(new BasicNameValuePair(CHAT_ID_KEY, CHAT_ID_VALUE));
        urlParameters.add(new BasicNameValuePair(TEXT_KEY, message));
        return urlParameters;
    }
}
